package com.vaibhav.startupheroesinternship.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Entity and a POJO to store details of User
 */
@Entity(tableName = "UserTable")
public class User {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "mobile_number")
    private String mobileNumber;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "sex")
    private String sex;

    @ColumnInfo(name = "age")
    private int age;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "password")
    private String password;

    // To store time of the last access (irrespective of right or wrong password)
    @ColumnInfo(name = "last_attempt")
    private long lastAttempt;

    // To keep track of wrong attemps.
    @ColumnInfo(name = "wrong_attempt_count")
    private int wrongAttemptCount;

    // To store path of image. Generally image should be uploaded to cloud and the link to it will
    // stored
    @ColumnInfo(name = "image")
    private String image;

    public User(@NonNull String mobileNumber, String name, String sex, int age, String email, String password, long lastAttempt, int wrongAttemptCount, String image) {
        this.mobileNumber = mobileNumber;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.email = email;
        this.password = password;
        this.lastAttempt = lastAttempt;
        this.wrongAttemptCount = wrongAttemptCount;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NonNull
    public String getMobileNumber() {
        if (mobileNumber == null)
            throw new IllegalStateException("Mobile number is null");
        return mobileNumber;
    }

    public void setMobileNumber(@NonNull String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getLastAttempt() {
        return lastAttempt;
    }

    public void setLastAttempt(long lastAttempt) {
        this.lastAttempt = lastAttempt;
    }

    public int getWrongAttemptCount() {
        return wrongAttemptCount;
    }

    public void setWrongAttemptCount(int wrongAttemptCount) {
        this.wrongAttemptCount = wrongAttemptCount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Name : " + name +
                " Age : " + age +
                " Sex : " + sex +
                " Email : " + email +
                " Mobile Number : " + mobileNumber +
                " Password : " + password +
                " Last Attempt : " + lastAttempt +
                " Wrong Attempt Count : " + wrongAttemptCount +
                " Image : " + image;
    }

}
