package com.vaibhav.startupheroesinternship;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vaibhav.startupheroesinternship.rss.RssNewsItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RssRecyclerViewAdapter extends RecyclerView.Adapter<RssRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = RssRecyclerViewAdapter.class.getName();
    private List<RssNewsItem> rssNewsItems;
    private Context context;

    RssRecyclerViewAdapter(Context context, List<RssNewsItem> rssNewsItems) {
        this.context = context;
        this.rssNewsItems = rssNewsItems;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, pubDate;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            pubDate = itemView.findViewById(R.id.item_pubDate);
        }
    }

    @NonNull
    @Override
    public RssRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_times_of_india, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.title.setText(rssNewsItems.get(holder.getAdapterPosition()).getTitle());
        holder.pubDate.setText(rssNewsItems.get(holder.getAdapterPosition()).getPubDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Link : " + rssNewsItems.get(holder.getAdapterPosition()).getLink());
                Intent openLink = new Intent(context, WebViewActivity.class);
                openLink.putExtra(WebViewActivity.LINK,
                        rssNewsItems.get(holder.getAdapterPosition()).getLink());
                context.startActivity(openLink);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rssNewsItems.size();
    }

    public void addItem(RssNewsItem item) {
        rssNewsItems.add(item);
        notifyItemInserted(rssNewsItems.size());
    }
}
