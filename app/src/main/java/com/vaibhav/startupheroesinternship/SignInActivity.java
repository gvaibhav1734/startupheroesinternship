package com.vaibhav.startupheroesinternship;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vaibhav.startupheroesinternship.models.User;

/**
 * Handles Sign In part of the app.
 */
public class SignInActivity extends AppCompatActivity {
    private static final String TAG = SignInActivity.class.getName();
    public static final int THRESHOLD_FAILED_ATTEMPTS = 3;
    public static final int THRESHOLD_FAILED_ATTEMPT_LOCKDOWN = 5 * 60 * 1000;  // in millis

    private EditText mobileNumber, password;
    private Button submit, signupButton;
    private Context context;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        context = this;
        Log.d(TAG, "Data : " + AppHelper.getInstance(context).getAppDatabase().userDao().get().toString());
        mobileNumber = findViewById(R.id.signin_et_mobile_number);
        password = findViewById(R.id.signin_et_password);
        submit = findViewById(R.id.signin_btn_submit);
        signupButton = findViewById(R.id.signin_btn_signup);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Data on Submit : " + AppHelper.getInstance(context).getAppDatabase().userDao().get().toString());
                boolean executeSignIn = true;
                String mobileNumberString = mobileNumber.getText().toString();
                String passwordString = password.getText().toString();
                {// Block to execute checks
                    if (mobileNumberString.equals("")) {
                        executeSignIn = false;
                        mobileNumber.setError("Enter mobile number (Required)");
                    } else if (!mobileNumberString.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$")) {
                        executeSignIn = false;
                        mobileNumber.setError("Wrong mobile number");
                    }
                    if (passwordString.equals("")) {
                        executeSignIn = false;
                    }
                }
                if (executeSignIn) {
                    if (userExists(mobileNumberString)) {
                        if (attemptSignIn(mobileNumberString, passwordString)) {
                            postSignIn();
                        }
                    } else {
                        Toast.makeText(context, "User doesnt not exist. Sign up.", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(context, SignupActivity.class);
                startActivity(signup);
            }
        });

    }

    /**
     * Checks whether user exists
     *
     * @param mobileNumber the mobile number of the user
     * @return true if mobile number (user) exists otherwise false
     */
    private boolean userExists(String mobileNumber) {
        return AppHelper.getInstance(context)
                .getAppDatabase().userDao().get(mobileNumber) != null;

    }

    /**
     * Attempts to sign in. Also handles updation of lastAttemptTime and wrongAttemptCount.
     *
     * @param mobileNumber the mobile number of the user
     * @param password     the password of the user
     * @return true if password matches with password in database otherwise false
     */
    private boolean attemptSignIn(String mobileNumber, String password) {
        User user = AppHelper.getInstance(context).getAppDatabase()
                .userDao().get(mobileNumber);
        boolean success = false;
        if (user.getWrongAttemptCount() < THRESHOLD_FAILED_ATTEMPTS) {
            // User's attempt to login hasn't crossed threshold so continue
            if (user.getPassword().equals(password)) {
                // On successful attempt reset wrong attempt counter.
                user.setWrongAttemptCount(0);
                success = true;
            } else {
                // Wrong password. Increment wrong attempt counter.
                user.setWrongAttemptCount(user.getWrongAttemptCount() + 1);
                Toast.makeText(context, "Mobile number/password wrong", Toast.LENGTH_SHORT)
                        .show();
            }
            // Update the last access time.
            user.setLastAttempt(System.currentTimeMillis());
            AppHelper.getInstance(context).getAppDatabase()
                    .userDao().update(user);
        } else {
            /** User's attempt to login crossed threshold lock his account for
             * {@value THRESHOLD_FAILED_ATTEMPT_LOCKDOWN} time.
             */
            long lastAttempt = user.getLastAttempt();
            if (System.currentTimeMillis() - lastAttempt > THRESHOLD_FAILED_ATTEMPT_LOCKDOWN) {
                // Threshold crossed. Reset counter. Repeat the attempt to check whether the password
                // entered now is correct.
                user.setWrongAttemptCount(0);
                user.setLastAttempt(System.currentTimeMillis());
                AppHelper.getInstance(context).getAppDatabase()
                        .userDao().update(user);
                return attemptSignIn(mobileNumber, password);
            } else {
                Toast.makeText(context, "Account locked for 5 minutes coz of 3 failed attempts.", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        return success;
    }

    /**
     * Closes this activity and opens HomeActivity to view the RssFeeds.
     */
    private void postSignIn() {
        finish();
        Intent login = new Intent(context, HomeActivity.class);
        startActivity(login);
    }

}
