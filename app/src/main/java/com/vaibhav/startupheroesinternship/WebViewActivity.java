package com.vaibhav.startupheroesinternship;

import android.content.Context;
import android.media.tv.TvContract;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

/**
 * To display the news article
 *
 * To load a link put the link in extras with {@value LINK} identifier.
 */
public class WebViewActivity extends AppCompatActivity {
    private static final String TAG = WebViewActivity.class.getName();
    public static final String LINK = "link";

    private Context context;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        context = this;
        WebView myWebView = findViewById(R.id.webview_wv);
        progressBar = findViewById(R.id.webview_pb);
        progressBar.setVisibility(View.VISIBLE);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                progressBar.setProgress(progress);
                if(progress == 100)
                    progressBar.setVisibility(View.GONE);
            }
        });
        myWebView.setWebViewClient(new WebViewClient());
        Bundle extras = getIntent().getExtras();
        String link = "https://www.google.com";
        if (extras != null) {
            if (extras.getString(LINK, null) != null)
                link = extras.getString(LINK);
        }
        myWebView.loadUrl(link);
    }

}
