package com.vaibhav.startupheroesinternship.database;

import com.vaibhav.startupheroesinternship.models.User;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

/**
 * All the operations that can be performed on UserTable in the database.
 */
@Dao
public interface UserDao {
    @Insert
    void insert(User user);

    @Insert
    void insert(List<User> userList);

    @Query("SELECT * FROM UserTable WHERE mobile_number=:mobileNumber")
    User get(String mobileNumber);

    @Query("SELECT * FROM UserTable")
    List<User> get();

    @Update
    void update(User user);

    @Delete
    void delete(User user);

    @Query("SELECT COUNT(*) FROM UserTable WHERE mobile_number=:mobileNumber")
    int size(String mobileNumber);
}
