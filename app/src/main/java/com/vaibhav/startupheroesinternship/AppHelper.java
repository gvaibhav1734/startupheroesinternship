package com.vaibhav.startupheroesinternship;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.vaibhav.startupheroesinternship.database.AppDatabase;

import androidx.room.Room;

/**
 * Helper class which stores singletons of database and RequestQueue(Volley)
 */
public class AppHelper {

    private static AppHelper instance;
    private Context context;
    private AppDatabase appDatabase;
    private RequestQueue requestQueue;


    private AppHelper(Context context) {
        this.context = context.getApplicationContext();
    }

    public static synchronized AppHelper getInstance(Context context) {
        if (instance == null) {
            instance = new AppHelper(context);
        }
        return instance;
    }

    public AppDatabase getAppDatabase() {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context, AppDatabase.class,
                    context.getString(R.string.database_name)).allowMainThreadQueries().build();
        }
        return appDatabase;
    }

    public void addToRequestQueue(Request request) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        requestQueue.add(request);
    }
}
