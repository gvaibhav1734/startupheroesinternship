package com.vaibhav.startupheroesinternship;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.eukaprotech.xmlprocessor.RssXmlProcessor;
import com.eukaprotech.xmlprocessor.RssXmlToHashMapListHandler;
import com.vaibhav.startupheroesinternship.rss.RssNewsItem;
import com.vaibhav.startupheroesinternship.rss.TimesOfIndiaItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Displays Rss Feed of news.
 */
public class HomeActivity extends AppCompatActivity {
    private static final String TAG = HomeActivity.class.getName();
    private static long backPressTime = 0;
    private static final int INACTIVE_THRESHOLD = 10 * 60 * 1000; // in millis

    private Context context;
    private RecyclerView recyclerView;
    private CountDownTimer inactiveTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        inactiveTimer = new CountDownTimer(INACTIVE_THRESHOLD, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "Timer ticking " + millisUntilFinished);
            }

            public void onFinish() {
                logout();
            }
        };
        inactiveTimer.start();
        recyclerView = findViewById(R.id.home_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "on Click ");
                // Start new counter coz user has made some kind of activity.
                inactiveTimer.cancel();
                inactiveTimer.start();
                return false;
            }
        });
        loadRssFeed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        inactiveTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        inactiveTimer.start();
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - backPressTime < 200) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("Logout?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing.
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                        }
                    });
            dialog.show();
        } else {
            backPressTime = System.currentTimeMillis();
            Toast.makeText(context, "Double tap back to logout", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadRssFeed() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                getString(R.string.url_times_of_india),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Response received " + response);
                        String entryTag = "item";
                        ArrayList<String> entryKeys = new ArrayList<>();
                        entryKeys.add("title");
                        entryKeys.add("description");
                        entryKeys.add("link");
                        entryKeys.add("pubDate");
                        entryKeys.add("guid");
                        RssXmlProcessor xmlProcessor = new RssXmlProcessor(entryTag, entryKeys);
                        xmlProcessor.execute(response, new RssXmlToHashMapListHandler() {
                            @Override
                            public void onStart() {

                            }

                            @Override
                            public void onSuccess(List<HashMap<String, String>> items) {
                                //consuming the processed items using the same keys
                                List<RssNewsItem> feedItems = new ArrayList<>();
                                RssRecyclerViewAdapter adapter = new RssRecyclerViewAdapter(context, feedItems);
                                recyclerView.setAdapter(adapter);
                                for (HashMap<String, String> item : items) {
                                    String title = item.get("title");
                                    String description = item.get("description");
                                    String link = item.get("link");
                                    String pubDate = item.get("pubDate");
                                    String guid = item.get("guid");
                                    adapter.addItem(new TimesOfIndiaItem(title, link, pubDate, description, guid));
                                }
                            }

                            @Override
                            public void onFail(Exception ex) {
                                Log.e(TAG, "XML Processor failed : " + ex.getMessage());
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Bad internet?", Toast.LENGTH_SHORT).show();
                    }
                });
        AppHelper.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void logout() {
        finish();
        inactiveTimer.cancel();
        inactiveTimer = null;
        Intent login = new Intent(context, SignInActivity.class);
        startActivity(login);
    }
}
