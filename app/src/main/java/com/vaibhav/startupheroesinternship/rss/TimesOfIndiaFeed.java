package com.vaibhav.startupheroesinternship.rss;

import java.util.ArrayList;
import java.util.List;

/**
 * RssFeed extended to support TimesOfIndia feed.
 */
public class TimesOfIndiaFeed extends RssFeed {

    private String language;
    private String lastBuildDate;
    private String copyright;
    private String docs;
    private ImageItem image;
    private List<TimesOfIndiaItem> items = new ArrayList<>();

    public TimesOfIndiaFeed(String title, String link, String description, String language, String lastBuildDate, String copyright, String docs, ImageItem image) {
        super(title, link, description);
        this.language = language;
        this.lastBuildDate = lastBuildDate;
        this.copyright = copyright;
        this.docs = docs;
        this.image = image;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getDocs() {
        return docs;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public ImageItem getImage() {
        return image;
    }

    public void setImage(ImageItem image) {
        this.image = image;
    }

    public List<TimesOfIndiaItem> getItems() {
        return items;
    }

    public void setItems(List<TimesOfIndiaItem> items) {
        this.items = items;
    }

    class ImageItem {
        private String title;
        private String url;
        private String link;

        public ImageItem(String title, String url, String link) {
            this.title = title;
            this.url = url;
            this.link = link;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
