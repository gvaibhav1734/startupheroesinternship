package com.vaibhav.startupheroesinternship.rss;

/**
 * Each item in the news feed can be represented by a POJO which has to implement this interface.
 */
public interface RssNewsItem {

    String getTitle();

    String getLink();

    String getPubDate();
}
