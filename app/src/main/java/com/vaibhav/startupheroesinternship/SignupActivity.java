package com.vaibhav.startupheroesinternship;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.vaibhav.startupheroesinternship.models.User;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Handles sign up part of app.
 */
public class SignupActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 222;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 333;
    private static final String TAG = SignupActivity.class.getName();
    private static final int CAMERA_REQUEST = 111;

    private Context context;
    private EditText name, age, mobileNumber, email, password;
    private Button submit, imageViewButton;
    private ImageView imageView;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final Spinner sex = findViewById(R.id.signup_spinner_sex);
        submit = findViewById(R.id.signup_btn_submit);
        mobileNumber = findViewById(R.id.signup_et_mobile_number);
        name = findViewById(R.id.signup_et_name);
        age = findViewById(R.id.signup_et_age);
        email = findViewById(R.id.signup_et_email);
        password = findViewById(R.id.signup_et_password);
        imageView = findViewById(R.id.signup_iv);
        imageViewButton = findViewById(R.id.signup_ib);

        imageViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryForPicture();
            }
        });
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sex, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sex.setAdapter(adapter);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean executeSignUp = true;
                String mobileNumberString = mobileNumber.getText().toString();
                String nameString = name.getText().toString();
                String sexString = sex.getSelectedItem().toString();
                int ageInt = 0;
                String emailString = email.getText().toString();
                String passwordString = password.getText().toString();

                {// Block to execute checks
                    if (nameString.equals("")) {
                        executeSignUp = false;
                    }
                    if (!age.getText().toString().equals("")) {
                        ageInt = Integer.parseInt(age.getText().toString());
                        if (ageInt <= 0) {
                            executeSignUp = false;
                            age.setError("Age has to be greater than 0");
                        }
                    }

                    // Regex to catch simple errors only. This is not the recommended approach.
                    // Recommended way is to send an email and get confirmation.
                    if (!emailString.equals("") && !emailString.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")) {
                        executeSignUp = false;
                        email.setError("Bad email address");
                    }
                    if (mobileNumberString.equals("")) {
                        executeSignUp = false;
                        mobileNumber.setError("Enter mobile number (Required)");
                    } else if (!mobileNumberString.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$")) {
                        executeSignUp = false;
                        mobileNumber.setError("Wrong mobile number");
                    }
                    if (password.getText().toString().equals("")) {
                        executeSignUp = false;
                        password.setError("Enter password (Required)");
                    }
                }

                if (executeSignUp) {
                    if (AppHelper.getInstance(context).getAppDatabase().userDao().size(mobileNumberString) == 0) {
                        User user = new User(mobileNumberString, nameString, sexString, ageInt,
                                emailString, passwordString,
                                System.currentTimeMillis(), 0,
                                imagePath);
                        AppHelper.getInstance(context).getAppDatabase().userDao().insert(user);
                        Toast.makeText(context, "User signed up!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, "Mobile number already exits. Try signing in.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    /**
     * Attempts to get a picture.
     */
    private void tryForPicture() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            photoFile = createImageFile();
            // Continue only if the File was successfully created
            if (photoFile != null) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        }
    }

    /**
     * Tries to create a file to store the image.
     *
     * @return File if its created otherwise null
     */
    private File createImageFile() {
        File image = null;
        // Create an image file name
        try {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                // Permission has already been granted
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                File storageDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES);
                image = File.createTempFile(
                        imageFileName,  // prefix
                        ".jpg",         // suffix
                        storageDir      // directory
                );

                // Save a file: path for use with ACTION_VIEW intents
                imagePath = "file:" + image.getAbsolutePath();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error IO : " + e.getMessage());
        }
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Picasso.get().load(imagePath).centerCrop().resize(400, 400)
                    .into(imageView);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA:
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Got permission yay so relay the request.
                    tryForPicture();
                } else {
                    // Do nothing since image is not mandatory.
                }
                return;
            }
        }
    }
}
